#ifndef SORTINGALGORTHMSOOP_ARRAYSORTRUNNER_H
#define SORTINGALGORTHMSOOP_ARRAYSORTRUNNER_H


#include "RunnerInterface.h"
#include "../Filler/FillerInterface.h"
#include "../Sorter/SorterInterface.h"
#include "../Printer/PrinterInterface.h"
#include "../TimeTracker.h"

class ArraySortRunner : public RunnerInterface {
private:
    FillerInterface *filler;
    SorterInterface *sorter;
    PrinterInterface *printer;

    int *array;
    size_t arraySize;

    TimeTracker timeTracker;
public:
    ArraySortRunner(FillerInterface &filler, SorterInterface &sorter, PrinterInterface &printer);

    void setFiller(FillerInterface &filler);
    void setSorter(SorterInterface &sorter);
    void setPrinter(PrinterInterface &printer);

    void setArray(int *array, size_t arraySize);

    void run() override;
};


#endif //SORTINGALGORTHMSOOP_ARRAYSORTRUNNER_H
