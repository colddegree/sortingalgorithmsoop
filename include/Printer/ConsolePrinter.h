#ifndef SORTINGALGORTHMSOOP_CONSOLEPRINTER_H
#define SORTINGALGORTHMSOOP_CONSOLEPRINTER_H


#include "PrinterInterface.h"

class ConsolePrinter : public PrinterInterface {
private:
    const char *delimiter;
public:
    explicit ConsolePrinter(const char *delimiter);

    void print(int *arr, size_t size) override;
};


#endif //SORTINGALGORTHMSOOP_CONSOLEPRINTER_H
