#ifndef SORTINGALGORTHMSOOP_COMPARATOR_H
#define SORTINGALGORTHMSOOP_COMPARATOR_H


#include "ComparatorInterface.h"
#include <functional>

class Comparator : public ComparatorInterface {
protected:
    std::function<int(const int a, const int b)> compareFunction;

public:
    int compare(int a, int b);

    ComparatorInterface& reversed();
};


#endif //SORTINGALGORTHMSOOP_COMPARATOR_H
