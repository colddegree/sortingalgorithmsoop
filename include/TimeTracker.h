#ifndef SORTINGALGORTHMSOOP_TIMETRACKER_H
#define SORTINGALGORTHMSOOP_TIMETRACKER_H


#include <chrono>

class TimeTracker {
private:
    std::chrono::steady_clock::time_point startTime;
    std::chrono::steady_clock::time_point endTime;

    bool running;
public:
    explicit TimeTracker();

    void startCount();
    void stopCount();

    double getElapsedTimeInSeconds();
};


#endif //SORTINGALGORTHMSOOP_TIMETRACKER_H
