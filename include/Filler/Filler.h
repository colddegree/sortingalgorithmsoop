#ifndef SORTINGALGORTHMSOOP_FILLER_H
#define SORTINGALGORTHMSOOP_FILLER_H


#include "FillerInterface.h"

class Filler : public FillerInterface {
protected:
    int *resource;
    size_t resourceSize;
public:
    explicit Filler(size_t resourceSize);

    virtual ~Filler();

    void fill(int *arr, size_t size) override;
};


#endif //SORTINGALGORTHMSOOP_FILLER_H
