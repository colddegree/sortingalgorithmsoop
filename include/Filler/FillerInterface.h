#ifndef SORTINGALGORTHMSOOP_FILLERINTERFACE_H
#define SORTINGALGORTHMSOOP_FILLERINTERFACE_H


#include <cstddef>

class FillerInterface {
public:
    virtual void fill(int *arr, size_t size) = 0;
    virtual void initResource() = 0;
};


#endif //SORTINGALGORTHMSOOP_FILLERINTERFACE_H
