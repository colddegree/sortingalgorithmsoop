#ifndef SORTINGALGORTHMSOOP_RANDOMFILLER_H
#define SORTINGALGORTHMSOOP_RANDOMFILLER_H


#include "Filler.h"

class RandomFiller : public Filler {
private:
    int low;
    int high;
public:
    RandomFiller(size_t resourceSize, int low, int high);

    void initResource() override;
};


#endif //SORTINGALGORTHMSOOP_RANDOMFILLER_H
