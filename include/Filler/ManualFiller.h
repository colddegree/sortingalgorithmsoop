#ifndef SORTINGALGORTHMSOOP_MANUALFILLER_H
#define SORTINGALGORTHMSOOP_MANUALFILLER_H


#include "Filler.h"

class ManualFiller : public Filler {
public:
    explicit ManualFiller(size_t resourceSize);

    void initResource() override;
};


#endif //SORTINGALGORTHMSOOP_MANUALFILLER_H
