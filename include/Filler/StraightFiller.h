#ifndef SORTINGALGORTHMSOOP_STRAIGHTFILLER_H
#define SORTINGALGORTHMSOOP_STRAIGHTFILLER_H


#include "Filler.h"

class StraightFiller : public Filler {
public:
    explicit StraightFiller(size_t resourceSize);

    void initResource() override;
};


#endif //SORTINGALGORTHMSOOP_STRAIGHTFILLER_H
