#ifndef SORTINGALGORTHMSOOP_SORTER_H
#define SORTINGALGORTHMSOOP_SORTER_H


#include "SorterInterface.h"
#include "../Comparator/ComparatorInterface.h"

class Sorter : public SorterInterface {
protected:
    ComparatorInterface &comparator;
public:
    explicit Sorter(ComparatorInterface &comparator);
};


#endif //SORTINGALGORTHMSOOP_SORTER_H
