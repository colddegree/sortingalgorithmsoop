#ifndef SORTINGALGORTHMSOOP_HEAPSORTER_H
#define SORTINGALGORTHMSOOP_HEAPSORTER_H


#include "Sorter.h"

class HeapSorter : public Sorter {
public:
    explicit HeapSorter(ComparatorInterface &comparator);

    void sort(int *arr, size_t size) override;

private:
    size_t heapSize;

    size_t left(size_t i);
    size_t right(size_t i);

    void maxHeapify(int *arr, size_t i);

    void buildMaxHeap(int *arr, size_t arrSize);
};


#endif //SORTINGALGORTHMSOOP_HEAPSORTER_H
