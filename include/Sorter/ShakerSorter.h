#ifndef SORTINGALGORTHMSOOP_SHAKERSORTER_H
#define SORTINGALGORTHMSOOP_SHAKERSORTER_H


#include "Sorter.h"

class ShakerSorter : public Sorter {
public:
    explicit ShakerSorter(ComparatorInterface &comparator);

    void sort(int *arr, size_t size) override;
};


#endif //SORTINGALGORTHMSOOP_SHAKERSORTER_H
