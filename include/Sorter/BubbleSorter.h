#ifndef SORTINGALGORTHMSOOP_BUBBLESORTER_H
#define SORTINGALGORTHMSOOP_BUBBLESORTER_H


#include "Sorter.h"

class BubbleSorter : public Sorter {
public:
    explicit BubbleSorter(ComparatorInterface &comparator);

private:
    void sort(int *arr, size_t size) override;
};


#endif //SORTINGALGORTHMSOOP_BUBBLESORTER_H
