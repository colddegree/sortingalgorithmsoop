#include "../include/TimeTracker.h"
#include <stdexcept>

TimeTracker::TimeTracker() {
    running = false;
}

void TimeTracker::startCount() {
    running = true;
    startTime = std::chrono::steady_clock::now();
}

void TimeTracker::stopCount() {
    if (!running)
        throw std::runtime_error("You need to startCount() first.");

    endTime = std::chrono::steady_clock::now();
    running = false;
}

double TimeTracker::getElapsedTimeInSeconds() {
    if (running)
        throw std::runtime_error("You need to stopCount() first.");

    return std::chrono::duration< double, std::ratio<1> >(endTime - startTime).count();
}
