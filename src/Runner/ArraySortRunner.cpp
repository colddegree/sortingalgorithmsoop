#include "../../include/Runner/ArraySortRunner.h"
#include <stdexcept>
#include <iostream>

ArraySortRunner::ArraySortRunner(FillerInterface &filler, SorterInterface &sorter, PrinterInterface &printer)
        : filler(&filler), sorter(&sorter), printer(&printer) {
    array = nullptr;
}

void ArraySortRunner::setFiller(FillerInterface &filler) {
    this->filler = &filler;
}

void ArraySortRunner::setSorter(SorterInterface &sorter) {
    this->sorter = &sorter;
}

void ArraySortRunner::setPrinter(PrinterInterface &printer) {
    this->printer = &printer;
}

void ArraySortRunner::setArray(int *array, size_t arraySize) {
    this->array = array;
    this->arraySize = arraySize;
}

void ArraySortRunner::run() {
    if (array == nullptr)
        throw std::runtime_error("Array is uninitialized.");

    const size_t PRINTING_LIMIT = 50;

    filler->fill(array, arraySize);

    if (arraySize <= PRINTING_LIMIT)
        printer->print(array, arraySize);

    timeTracker.startCount();
    sorter->sort(array, arraySize);
    timeTracker.stopCount();

    std::cout << "Sorting duration: " << timeTracker.getElapsedTimeInSeconds() << " sec" << std::endl;

    if (arraySize <= PRINTING_LIMIT)
        printer->print(array, arraySize);

    std::cout << std::endl;
}
