#include "../../include/Printer/ConsolePrinter.h"
#include <iostream>

ConsolePrinter::ConsolePrinter(const char *delimiter) : delimiter(delimiter) {}

void ConsolePrinter::print(int *arr, size_t size) {

    for (size_t i = 0; i < size - 1; ++i) {
        std::cout << arr[i] << delimiter;
    }
    std::cout << arr[size - 1] << std::endl;
}
