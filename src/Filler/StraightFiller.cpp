#include "../../include/Filler/StraightFiller.h"

StraightFiller::StraightFiller(size_t resourceSize) : Filler(resourceSize) {}

void StraightFiller::initResource() {
    for (size_t i = 0; i < resourceSize; ++i) {
        resource[i] = i + 1;
    }
}
