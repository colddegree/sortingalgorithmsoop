#include "../../include/Filler/ManualFiller.h"
#include <iostream>

ManualFiller::ManualFiller(size_t resourceSize) : Filler(resourceSize) {}

void ManualFiller::initResource() {
    std::cout << "Enter " << resourceSize << "element(s):" << std::endl;

    for (size_t i = 0; i < resourceSize; ++i) {
        std::cin >> resource[i];
    }
}
