#include "../../include/Filler/RandomFiller.h"
#include <cstdlib> // rand

RandomFiller::RandomFiller(size_t resourceSize, int low, int high)
        : Filler(resourceSize), low(low), high(high) {
    initResource();
}

void RandomFiller::initResource() {
    for (size_t i = 0; i < resourceSize; ++i)
        resource[i] = rand() % high + low;
}
