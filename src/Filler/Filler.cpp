#include "../../include/Filler/Filler.h"
#include <stdexcept>

Filler::Filler(size_t resourceSize) : resourceSize(resourceSize) {
    resource = new int[resourceSize];
}

Filler::~Filler() {
    delete [] resource;
}

void Filler::fill(int *arr, size_t size) {
    if (size != resourceSize)
        throw std::length_error("Array size and resource size must be equal.");

    for (size_t i = 0; i < size; ++i) {
        arr[i] = resource[i];
    }
}