#include "../../include/Sorter/ShakerSorter.h"
#include <algorithm> // std::swap

ShakerSorter::ShakerSorter(ComparatorInterface &comparator) : Sorter(comparator) {}

void ShakerSorter::sort(int *arr, size_t size) {
    size_t left = 0;
    size_t right = size - 1;

    while (left < right) {
        for (size_t i = left; i < right; ++i) {
            if ( comparator.compare(arr[i + 1], arr[i]) < 0 )
                std::swap(arr[i], arr[i + 1]);
        }
        --right;

        for (size_t i = right; i > left; --i) {
            if ( comparator.compare(arr[i - 1], arr[i]) > 0 )
                std::swap(arr[i - 1], arr[i]);
        }
        ++left;
    }
}
