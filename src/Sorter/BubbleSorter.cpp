#include "../../include/Sorter/BubbleSorter.h"
#include <algorithm> // std::swap

BubbleSorter::BubbleSorter(ComparatorInterface &comparator) : Sorter(comparator) {}

void BubbleSorter::sort(int *arr, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        bool swapped = false;
        for (size_t j = 0; j < size-i-1; ++j) {
            if ( comparator.compare(arr[j], arr[j+1]) > 0 ) {
                std::swap(arr[j], arr[j+1]);
                swapped = true;
            }
        }
        if (!swapped) break;
    }
}
