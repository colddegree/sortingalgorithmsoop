#include "../../include/Sorter/HeapSorter.h"
#include <algorithm> // std::swap
#include <cmath> // trunc

HeapSorter::HeapSorter(ComparatorInterface &comparator) : Sorter(comparator) {}

void HeapSorter::sort(int *arr, size_t size) {
    buildMaxHeap(arr, size);

    for (size_t i = size - 1; i >= 1; i--) {
        std::swap(arr[0], arr[i]);
        heapSize -= 1;
        maxHeapify(arr, 0);
    }
}

size_t HeapSorter::left(size_t i) {
    return 2*i + 1;
}

size_t HeapSorter::right(size_t i) {
    return 2*i + 2;
}

void HeapSorter::maxHeapify(int *arr, size_t i) {

    size_t l = left(i);
    size_t r = right(i);

    size_t largest;

    if ( (l < heapSize) && (comparator.compare(arr[l], arr[i]) > 0) ) {
        largest = l;
    } else {
        largest = i;
    }

    if ( (r < heapSize) && (comparator.compare(arr[r], arr[largest]) > 0) ) {
        largest = r;
    }

    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        maxHeapify(arr, largest);
    }
}

void HeapSorter::buildMaxHeap(int *arr, size_t arrSize) {

    heapSize = arrSize;

    for (size_t i = static_cast<size_t>( trunc(arrSize / 2) ); (i >= 0) && (i < arrSize); i--) {
        maxHeapify(arr, i);
    }
}