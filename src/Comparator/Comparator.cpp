#include "../../include/Comparator/Comparator.h"

int Comparator::compare(int a, int b) {
    return compareFunction(a, b);
}

ComparatorInterface& Comparator::reversed() {

    auto *reversedComparator = new Comparator;

    reversedComparator->compareFunction = [this](int a, int b) -> int {
        return -compare(a, b);
    };

    return *reversedComparator; // memory leak!
}
