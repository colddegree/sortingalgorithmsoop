#include <iostream>
#include <ctime>
#include "include/Comparator/ComparatorInterface.h"
#include "include/Comparator/AscendingComparator.h"
#include "include/Sorter/ShakerSorter.h"
#include "include/Sorter/BubbleSorter.h"
#include "include/Sorter/HeapSorter.h"
#include "include/Filler/RandomFiller.h"
#include "include/Printer/ConsolePrinter.h"
#include "include/Runner/ArraySortRunner.h"

using namespace std;

int main() {

    srand(time(NULL));

    size_t arrSize = 10000;
    int *arr = new int[arrSize];

    AscendingComparator ascComparator;

    ShakerSorter shakerSorter{ascComparator};
    BubbleSorter bubbleSorter{ascComparator};
    HeapSorter heapSorter{ascComparator};

    RandomFiller randomFiller{arrSize, -5, 20};
    ConsolePrinter consolePrinter{", "};


    ArraySortRunner runner{randomFiller, shakerSorter, consolePrinter};
    runner.setArray(arr, arrSize);

    cout << "ShakerSort:" << endl;
    runner.run();


    cout << "BubbleSort:" << endl;
    runner.setSorter(bubbleSorter);
    runner.run();

    cout << "HeapSort:" << endl;
    runner.setSorter(heapSorter);
    runner.run();


    delete [] arr;

    return 0;
}
